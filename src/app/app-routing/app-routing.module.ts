import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { AccountsComponent } from '../pages/accounts/accounts.component';

import { AliasTeamsComponent } from '../pages/aliasTeams/aliasTeams.component';
import { BetTypesComponent } from '../pages/betTypes/betTypes.component';
import { CompetitionsComponent } from '../pages/competitions/competitions.component';
import { CountriesComponent } from '../pages/countries/countries.component';
import { SportsComponent } from '../pages/sports/sports.component';
import { UsersComponent } from '../pages/users/users.component';

import { SportsbooksComponent } from '../pages/sportsbooks/sportsbooks.component';
import { TeamsComponent } from '../pages/teams/teams.component';
import { MatchesComponent } from '../pages/matches/matches.component';
import { OddsComponent } from '../pages/odds/odds.component';
import { BetsComponent } from '../pages/bets/bets.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
    },
    {
        path: 'accounts',
        component: AccountsComponent,
    },
    {
        path: 'aliasTeams',
        component: AliasTeamsComponent,
    },
    {
        path: 'betTypes',
        component: BetTypesComponent,
    },
    {
        path: 'competitions',
        component: CompetitionsComponent,
    },
    {
        path: 'countries',
        component: CountriesComponent,
    },
    {
        path: 'users',
        component: UsersComponent,
    },
    {
        path: 'sports',
        component: SportsComponent,
    },
    {
        path: 'sportsbooks',
        component: SportsbooksComponent,
    },
    {
        path: 'teams',
        component: TeamsComponent,
    },
    {
        path: 'matches',
        component: MatchesComponent
    },
    {
        path: 'odds',
        component: OddsComponent
    },
    {
        path: 'bets',
        component: BetsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
