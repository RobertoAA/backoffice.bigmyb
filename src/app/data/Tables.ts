export const Tables = [
    {
        name: 'accounts'
    },
    {
        name: 'alias_teams'
    },
    {
        name: 'bets'
    },
    {
        name: 'bet_types'
    },
    {
        name: 'competitions'
    },
    {
        name: 'countries'
    },
    {
        name: 'matches'
    },
    {
        name: 'odds'
    },
    {
        name: 'sports'
    },
    {
        name: 'sportsbooks'
    },
    {
        name: 'teams'
    },
    {
        name: 'users'
    }
]