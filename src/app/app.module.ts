import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component'

import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { AccountService } from './pages/accounts/account.service';
import { AliasTeamsComponent } from './pages/aliasTeams/aliasTeams.component';
import { AliasTeamService } from './pages/aliasTeams/aliasTeam.service';
import { BetsComponent } from './pages/bets/bets.component';
import { BetService } from './pages/bets/bet.service';
import { BetTypesComponent } from './pages/betTypes/betTypes.component';
import { BetTypeService } from './pages/betTypes/betType.service';
import { CompetitionsComponent } from './pages/competitions/competitions.component';
import { CompetitionService } from './pages/competitions/competition.service';
import { CountriesComponent } from './pages/countries/countries.component';
import { CountryService } from './pages/countries/country.service';
import { MatchesComponent } from './pages/matches/matches.component';
import { MatchService } from './pages/matches/match.service';
import { OddsComponent } from './pages/odds/odds.component';
import { OddService } from './pages/odds/odd.service';
import { SportsComponent } from './pages/sports/sports.component';
import { SportService } from './pages/sports/sport.service';
import { SportsbooksComponent } from './pages/sportsbooks/sportsbooks.component';
import { SportsbookService } from './pages/sportsbooks/sportsbook.service';
import { TeamsComponent } from './pages/teams/teams.component';
import { TeamService } from './pages/teams/team.service';
import { UsersComponent } from './pages/users/users.component';
import { UserService } from './pages/users/user.service';

import { AppRoutingModule } from './app-routing/app-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule }   from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AccountsComponent,
    AliasTeamsComponent,
    BetsComponent,
    BetTypesComponent,
    CompetitionsComponent,
    CountriesComponent,
    MatchesComponent,
    OddsComponent,
    SportsComponent,
    SportsbooksComponent,
    TeamsComponent,
    UsersComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    FormsModule,
  ],
  providers: [
    AccountService,
    AliasTeamService,
    BetService,
    BetTypeService,
    CompetitionService,
    CountryService,
    MatchService,
    OddService,
    SportService,
    SportsbookService,
    TeamService,
    UserService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
