import { Component, OnInit, ViewChild } from '@angular/core';

import { AliasTeam } from './aliasTeam';
import { AliasTeamService } from './aliasTeam.service';

import { Sport } from './../sports/sport';
import { SportService } from './../sports/sport.service';

import { Country } from './../countries/country';
import { CountryService } from './../countries/country.service'

import { Team } from './../teams/team';
import { TeamService } from './../teams/team.service'

@Component({
  selector: 'app-aliasTeams',
  templateUrl: './aliasTeams.component.html',
  styleUrls: ['./aliasTeams.component.css']
})
export class AliasTeamsComponent implements OnInit {
  @ViewChild('edit_team_id') edit_team_id;
  @ViewChild('edit_name') edit_name;
  @ViewChild('edit_web') edit_web;
  aliasTeams: AliasTeam[];
  sports: Sport[];
  countries: Country[];
  teams: any[];
  sportSelected: number;
  countrySelected: number;
  multipleWeb: string;

  constructor(
    private aliasTeamService: AliasTeamService,
    private sportService: SportService,
    private countryService: CountryService,
    private teamService: TeamService
  ) { }

  ngOnInit() {
    this.getAliasTeams();
    this.getSports();
    this.getCountries();
  }

  getAliasTeams(): void {
    this.aliasTeamService
      .getAliasTeams()
      // .then( function (sportsbooks) { this.sportsbooks = sportsbooks; console.log(sportsbooks); });
      .then(aliasTeams => {
        this.aliasTeams = aliasTeams; 
        this.aliasTeams.forEach( function(obj){ obj.editing = false; } )
      });
  }

  getSports(): void{
    this.sportService
      .getSports()
      .then(sports => {
        this.sports = sports;
      })
  }

  getCountries(): void{
    this.countryService
      .getCountries()
      .then(countries => {
        this.countries = countries;
      })
  }

  editAliasTeam(aliasTeams: AliasTeam){
    aliasTeams.editing = true;
  }
  
  stopEditAliasTeam(aliasTeams: AliasTeam): void {
    aliasTeams.editing = false;
  }

  sendEditAliasTeam( id, new_team_id, new_name, new_web): void{
    this.aliasTeamService
      .update( 
        id,
        new_team_id.nativeElement.value, 
        new_name.nativeElement.value, 
        new_web.nativeElement.value, 
      )
      .then( (res)=>{  console.log(res); this.getAliasTeams(); });
  }

  addNewAliasTeam( event, new_team_id, new_name, new_web): void{
    event.preventDefault();
    this.aliasTeamService
      .create(new_team_id, new_name, new_web)
      .then( (res)=>{ console.log(res); this.getAliasTeams(); });

  }

  selectSport(id){
    this.sportSelected = id;
    this.getTeams();
  }

  selectCountry(id){
    this.countrySelected = id;
    this.getTeams();
  }

  getTeams(){
    let params = '';
    if(this.countrySelected != 0 && this.countrySelected != undefined && this.sportSelected != 0 && this.sportSelected != undefined){
      params += '/country_id/' + this.countrySelected;
      params += '/sport_id/' + this.sportSelected;
      this.teamService
        .getTeams(params)
        .then(teams => {
          this.teams = teams;
        })
    }
  }

  closeMultiple(){
    this.countrySelected = undefined;
    this.sportSelected = undefined;
    this.teams = undefined;
  }

  saveMultiple(){
    console.log(this.teams);
    this.teams.forEach( team => {
      if(team.alias_name !== undefined){
        this.aliasTeamService
          .create(team.id, team.alias_name, this.multipleWeb)
          .then( (res)=>{ console.log(res); this.getAliasTeams(); });
      }
    })
    this.closeMultiple();
  }

}
