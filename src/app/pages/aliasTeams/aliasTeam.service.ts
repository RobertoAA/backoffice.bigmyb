import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { AliasTeam } from './aliasteam';

@Injectable()
export class AliasTeamService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost/api.bigmyb/public/aliasTeams';  // URL to web api
  private url_create = 'http://localhost/api.bigmyb/public/aliasTeam';  // URL to web api

  constructor(private http: Http) { }

  getAliasTeams(): Promise<AliasTeam[]> {
    return this.http.get(this.url)
               .toPromise()
               // .then(response => console.log(response.json()))
               .then(response => response.json() as AliasTeam[])
               .catch(this.handleError);
  }


  getAliasTeam(id: number): Promise<AliasTeam> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as AliasTeam)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create( team_id: number, name: string, web: string ): Promise<AliasTeam> {
    return this.http
      .post(this.url_create, JSON.stringify({ team_id: team_id, name: name, web: web}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as AliasTeam)
      .catch(this.handleError);
  }

  update(id: number, team_id: number, name: string, web: string): Promise<AliasTeam> {
    const url = `${this.url_create}/${id}`;
    return this.http
      .put(url, JSON.stringify({id: id, team_id: team_id, name: name, web: web}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as AliasTeam)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

