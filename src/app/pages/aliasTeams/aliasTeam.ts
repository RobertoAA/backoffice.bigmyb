export class AliasTeam {
    id: number;
    team_id: number;
    name: string;
    web: string;
    editing: boolean;
}
