import { Component, OnInit, ViewChild } from '@angular/core';

import { Sport } from './sport';
import { SportService } from './sport.service';

@Component({
  selector: 'app-sport',
  templateUrl: './sports.component.html',
  styleUrls: ['./sports.component.css']
})
export class SportsComponent implements OnInit {
  @ViewChild('edit_name') edit_name;
  sports: Sport[];

  constructor(
    private sportService: SportService
  ) { }

  getSports(): void {
    this.sportService
      .getSports()
      // .then( function (sportsbooks) { this.sportsbooks = sportsbooks; console.log(sportsbooks); });
      .then(sports => {
        this.sports = sports; 
        this.sports.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getSports();
  }

  editSport(sport: Sport){
    sport.editing = true;
  }
  
  stopEditSport(sport: Sport): void {
    sport.editing = false;
  }

  sendEditSport( id, new_name): void{
    this.sportService
      .update( 
        id,
        new_name.nativeElement.value, 
      )
      .then( (res)=>{  console.log(res); this.getSports(); });
  }

  addNewSport( event, new_name): void{
    event.preventDefault();
    this.sportService
      .create(new_name)
      .then( (res)=>{ console.log(res); this.getSports(); });

  }

}
