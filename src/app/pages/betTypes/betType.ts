export class BetType {
    id: number;
    name: string;
    options: string;
    sport_id: number;
    editing: boolean;
}
