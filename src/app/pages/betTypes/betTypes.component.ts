import { Component, OnInit, ViewChild } from '@angular/core';

import { BetType } from './betType';
import { BetTypeService } from './betType.service';

@Component({
  selector: 'app-betTypes',
  templateUrl: './betTypes.component.html',
  styleUrls: ['./betTypes.component.css']
})
export class BetTypesComponent implements OnInit {
  @ViewChild('edit_team_name') edit_team_name;
  @ViewChild('edit_team_sport') edit_team_sport;
  betTypes: BetType[];

  constructor(
    private betTypeService: BetTypeService
  ) { }

  getBetTypes(): void {
    this.betTypeService
      .getBetTypes()
      // .then( function (sportsbooks) { this.sportsbooks = sportsbooks; console.log(sportsbooks); });
      .then(betTypes => {
        this.betTypes = betTypes; 
        this.betTypes.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getBetTypes();
  }

  editBetType(betType: BetType){
    betType.editing = true;
  }
  
  stopEditBetType(betType: BetType): void {
    betType.editing = false;
  }

  sendEditBetType( id, new_name, new_options, new_sport_id): void{
    this.betTypeService
      .update( 
        id,
        new_name.nativeElement.value, 
        new_options.nativeElement.value, 
        new_sport_id.nativeElement.value
      )
      .then( (res)=>{  console.log(res); this.getBetTypes(); });
  }

  addNewBetType( event, new_name, new_options, new_sport_id): void{
    event.preventDefault();
    this.betTypeService
      .create(new_name, new_options, new_sport_id)
      .then( (res)=>{ console.log(res); this.getBetTypes(); });

  }

}
