import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { BetType } from './betType';

@Injectable()
export class BetTypeService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost/api.bigmyb/public/betTypes';  // URL to web api
  private url_create = 'http://localhost/api.bigmyb/public/betType';  // URL to web api

  constructor(private http: Http) { }

  getBetTypes(): Promise<BetType[]> {
    return this.http.get(this.url)
               .toPromise()
               // .then(response => console.log(response.json()))
               .then(response => response.json() as BetType[])
               .catch(this.handleError);
  }


  getBetType(id: number): Promise<BetType> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as BetType)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string, options: string, sport_id: number): Promise<BetType> {
    return this.http
      .post(this.url_create, JSON.stringify({name: name, options: options, sport_id: sport_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as BetType)
      .catch(this.handleError);
  }

  update(id: number, name: string, options: string, sport_id: number): Promise<BetType> {
    const url = `${this.url_create}/${id}`;
    return this.http
      .put(url, JSON.stringify({id: id, name: name, options: options, sport_id: sport_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as BetType)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

