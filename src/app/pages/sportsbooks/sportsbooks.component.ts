import { Component, OnInit, ViewChild } from '@angular/core';

import { Sportsbook } from './sportsbook';
import { SportsbookService } from './sportsbook.service';

@Component({
  selector: 'app-sportsbooks',
  templateUrl: './sportsbooks.component.html',
  styleUrls: ['./sportsbooks.component.css']
})
export class SportsbooksComponent implements OnInit {
  @ViewChild('edit_sportsbook_name') edit_sportsbook_name;
  sportsbooks: Sportsbook[];

  constructor(
    private sportsbookService: SportsbookService
  ) { }

  getSportsbooks(): void {
    this.sportsbookService
      .getSportsbooks()
      .then(sportsbooks => {
        this.sportsbooks = sportsbooks; 
        this.sportsbooks.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getSportsbooks();
  }

  editSportsbook(sportsbook: Sportsbook): void {
    sportsbook.editing = true;
  }

  stopEditSportsbook(sportsbook: Sportsbook): void {
    sportsbook.editing = false;
  }

  sendEditSportsbook( id, new_sportsbook_name): void{
    this.sportsbookService
      .update( id, new_sportsbook_name.nativeElement.value)
      .then( (res)=>{  console.log(res); this.getSportsbooks(); });
  }

  addNewSportsbook( event, new_sportsbook_name): void{
    event.preventDefault();
    this.sportsbookService
      .create(new_sportsbook_name)
      .then( (res)=>{  console.log(res); this.getSportsbooks(); });

  }

}
