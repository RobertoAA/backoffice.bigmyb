export class Team {
    id: number;
    name: string;
    sport_id: number;
    editing: boolean;
}
