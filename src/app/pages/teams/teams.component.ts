import { Component, OnInit, ViewChild } from '@angular/core';

import { Team } from './team';
import { TeamService } from './team.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  @ViewChild('edit_team_name') edit_team_name;
  @ViewChild('edit_team_sport_id') edit_team_sport_id;
  @ViewChild('edit_team_country_id') edit_team_country_id;
  teams: Team[];

  constructor(
    private teamService: TeamService
  ) { }

  getTeams(): void {
    this.teamService
      .getTeams()
      .then(teams => {
        this.teams = teams; 
        this.teams.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getTeams();
  }

  editTeam(team: Team){
    team.editing = true;
  }
  
  stopEditTeam(team: Team): void {
    team.editing = false;
  }

  sendEditTeam( id, new_team_name, new_team_sport_id, new_team_country_id): void{
    this.teamService
      .update( 
        id,
        new_team_name.nativeElement.value, 
        new_team_sport_id.nativeElement.value,
        new_team_country_id.nativeElement.value,
      )
      .then( (res)=>{  console.log(res); this.getTeams(); });
  }

  addNewTeam( event, new_team_name, new_team_sport_id, new_team_country_id): void{
    event.preventDefault();
    this.teamService
      .create(new_team_name, new_team_sport_id, new_team_country_id)
      .then( (res)=>{ console.log(res); this.getTeams(); });

  }

}
