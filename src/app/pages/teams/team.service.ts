import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Team } from './team';

import { environment } from './../../../environments/environment';

@Injectable()
export class TeamService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = environment.api_url + '/teams';  // URL to web api
  private url_ind = environment.api_url + '/team';  // URL to web api

  constructor(private http: Http) { }

  getTeams(params = ''): Promise<Team[]> {
    let url = this.url;
    if(params !== ''){
      url += '/search' + params;
    }
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Team[])
      .catch(this.handleError);
  }


  getTeam(id: number): Promise<Team> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Team)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string, sport_id: number, country_id: number): Promise<Team> {
    return this.http
      .post(this.url_ind, JSON.stringify({name: name, sport_id: sport_id, country_id: country_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Team)
      .catch(this.handleError);
  }

  update(id: number, name: string, sport_id: number, country_id: number): Promise<Team> {
    const url = `${this.url_ind}/${id}`;
    return this.http
      .put(url, JSON.stringify({id: id, name: name, sport_id: sport_id, country_id: country_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Team)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

