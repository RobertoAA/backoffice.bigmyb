import { Component, OnInit, ViewChild } from '@angular/core';

import { Competition } from './competition';
import { CompetitionService } from './competition.service';

@Component({
  selector: 'app-competition',
  templateUrl: './competitions.component.html',
  styleUrls: ['./competitions.component.css']
})
export class CompetitionsComponent implements OnInit {
  @ViewChild('edit_name') edit_name;
  competitions: Competition[];

  constructor(
    private competitionService: CompetitionService
  ) { }

  getCompetitions(): void {
    this.competitionService
      .getCompetitions()
      // .then( function (sportsbooks) { this.sportsbooks = sportsbooks; console.log(sportsbooks); });
      .then(competitions => {
        this.competitions = competitions; 
        this.competitions.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getCompetitions();
  }

  editCompetition(competition: Competition){
    competition.editing = true;
  }
  
  stopEditCompetition(competition: Competition): void {
    competition.editing = false;
  }

  sendEditCompetition( id, new_name): void{
    this.competitionService
      .update( 
        id,
        new_name.nativeElement.value, 
      )
      .then( (res)=>{  console.log(res); this.getCompetitions(); });
  }

  addNewCompetition( event, new_name): void{
    event.preventDefault();
    this.competitionService
      .create(new_name)
      .then( (res)=>{ console.log(res); this.getCompetitions(); });

  }

}
