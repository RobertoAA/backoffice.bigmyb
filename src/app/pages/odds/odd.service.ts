import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Odd } from './odd';

import { environment } from './../../../environments/environment';

@Injectable()
export class OddService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = environment.api_url + '/odds';  // URL to web api
  private url_ind = environment.api_url + '/odd';  // URL to web api

  constructor(private http: Http) { }

  getOdds(match_id = 0): Promise<Odd[]> {
    let options = '';
    if(match_id !== 0){
      options = '/search/match/' + match_id;
    }else{
      options = '/extra';
    }
    const url = this.url + options;
    return this.http.get(url)
               .toPromise()
               // .then(response => console.log(response.json()))
               .then(response => response.json() as Odd[])
               .catch(this.handleError);
  }


  getOdd(id: number): Promise<Odd> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Odd)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url_ind}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(match_id: number, bet_type_id: number, value: string, datetime: string, datetime_last: string, sportsbook_id: number, result: number): Promise<Odd> {
    return this.http
      .post(this.url_ind, JSON.stringify({ match_id: match_id, bet_type_id: bet_type_id, value: value, datetime: datetime, datetime_last: datetime_last, sportsbook_id: sportsbook_id, result: result}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Odd)
      .catch(this.handleError);
  }

  update(id: number, match_id: number, bet_type_id: number, value: string, datetime: string, datetime_last: string, sportsbook_id: number, result: number): Promise<Odd> {
    const url = `${this.url_ind}/${id}`;
    return this.http
      .put(url, JSON.stringify({id: id, match_id: match_id, bet_type_id: bet_type_id, value: value, datetime: datetime, datetime_last: datetime_last, sportsbook_id: sportsbook_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Odd)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

