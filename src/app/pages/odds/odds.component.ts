import { Component, OnInit, ViewChild } from '@angular/core';

import { Odd } from './odd';
import { OddService } from './odd.service';

@Component({
  selector: 'app-odds',
  templateUrl: './odds.component.html',
  styleUrls: ['./odds.component.css']
})
export class OddsComponent implements OnInit {
  @ViewChild('edit_odd_match_id') edit_odd_match_id;
  @ViewChild('edit_odd_bet_type_id') edit_odd_bet_type_id;
  @ViewChild('edit_odd_value') edit_odd_value;
  @ViewChild('edit_odd_datetime') edit_odd_datetime;
  @ViewChild('edit_odd_datetime_last') edit_odd_datetime_last;
  @ViewChild('edit_odd_sportsbook_id') edit_odd_sportsbook_id;
  @ViewChild('edit_odd_result') edit_odd_result;
  odds: Odd[];

  constructor(
    private oddService: OddService
  ) { }

  getOdds(): void {
    this.oddService
      .getOdds()
      .then(odds => {
        this.odds = odds;
        this.odds.forEach( function(obj){ obj.editing = false; } );
      });
  }

  ngOnInit() {
    this.getOdds();
  }

  editOdd(odd: Odd): void {
    odd.editing = true;
  }

  stopEditOdd(odd: Odd): void {
    odd.editing = false;
  }

  sendEditOdd( 
    id,
    new_odd_match_id,
    new_odd_bet_type_id,
    new_odd_value,
    new_odd_datetime,
    new_odd_datetime_last,
    new_odd_sportsbook_id,
    new_odd_result
  ): void{
    this.oddService
      .update(
        id,
        new_odd_match_id.nativeElement.value,
        new_odd_bet_type_id.nativeElement.value,
        new_odd_value.nativeElement.value,
        new_odd_datetime.nativeElement.value,
        new_odd_datetime_last.nativeElement.value,
        new_odd_sportsbook_id.nativeElement.value,
        new_odd_result.nativeElement.value
      )
      .then( (res) => {  console.log(res); this.getOdds(); });
  }

  addNewOdd( event, new_odd_match_id, new_odd_bet_type_id, new_odd_value, new_odd_datetime, new_odd_datetime_last, new_odd_sportsbook_id, new_odd_result): void{
    event.preventDefault();
    this.oddService
      .create(new_odd_match_id, new_odd_bet_type_id, new_odd_value, new_odd_datetime, new_odd_datetime_last, new_odd_sportsbook_id, new_odd_result)
      .then( (res) => {  
        console.log(res); 
        this.getOdds()
        ; });

  }

}
