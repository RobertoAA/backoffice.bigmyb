export class Odd {
    id: number;
    match_id: number;
    bet_type_id: number;
    value: string;
    datetime: string;
    datetime_last: string;
    sportsbook_id: number;
    result: number;
    home_team_name?: string;
    away_team_name?: string;
    bet_type_name?: string;
    sportsbook_name?: string;
    editing: boolean;
}
