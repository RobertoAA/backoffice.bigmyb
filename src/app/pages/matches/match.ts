export class Match {
    id: number;
    home_team_id: number;
    away_team_id: number;
    datetime: string;
    competition_id: number;
    result: string;
    home_team_name?: string;
    away_team_name?: string;
    competition_name?: string;
    editing?: boolean;
    deleting?: boolean;
}
