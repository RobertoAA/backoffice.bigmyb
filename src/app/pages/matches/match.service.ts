import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Match } from './match';

import { environment } from './../../../environments/environment';

@Injectable()
export class MatchService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = environment.api_url + '/matches';  // URL to web api
  private url_ind = environment.api_url + '/match';  // URL to web api

  constructor(private http: Http) { }

  getMatches(): Promise<Match[]> {
    console.warn(this.url);
    const url = this.url + '/extra';
    return this.http.get(url)
               .toPromise()
               // .then(response => console.log(response.json()))
               .then(response => response.json() as Match[])
               .catch(this.handleError);
  }


  getMatch(id: number): Promise<Match> {
    const url = `${this.url_ind}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Match)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url_ind}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then((e) => { console.log(e); })
      .catch(this.handleError);
  }

  create(home_team_id: number, away_team_id: number, datetime: string, competition_id: number, result: string): Promise<Match> {
    return this.http
      .post(this.url_ind, JSON.stringify({home_team_id: home_team_id, away_team_id: away_team_id, datetime: datetime, competition_id: competition_id, result: result}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Match)
      .catch(this.handleError);
  }

  update(id: number, home_team_id: number, away_team_id: number, datetime: string, competition_id: number, result): Promise<Match> {
    const url = `${this.url_ind}/${id}`;
    return this.http
      .put(url, JSON.stringify({ id: id, home_team_id: home_team_id, away_team_id: away_team_id, datetime: datetime, competition_id: competition_id, result: result}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Match)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

