import { Component, OnInit, ViewChild } from '@angular/core';

import { Match } from './match';
import { MatchService } from './match.service';

import { Odd } from './../odds/odd';
import { OddService } from './../odds/odd.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {
  @ViewChild('edit_match_home_team_id') edit_match_home_team_id;
  @ViewChild('edit_match_away_team_id') edit_match_away_team_id;
  @ViewChild('edit_match_datetime') edit_match_datetime;
  @ViewChild('edit_match_competition_id') edit_match_competition_id;
  @ViewChild('edit_match_result') edit_match_result;
  matches: Match[];
  match_deleting: Match;
  receptor_match: Match;
  odds_deleting: Odd[];

  constructor(
    private matchService: MatchService,
    private oddService: OddService
  ) { }

  getMatches(): void {
    this.matchService
      .getMatches()
      .then(matches => {
        this.matches = matches;
        this.matches.forEach( function(obj){ obj.editing = false; obj.deleting = false; } );
      });
  }

  getMatch(id): void{
    console.log(id);
    this.matchService
      .getMatch(id)
      .then(match => {
        this.receptor_match = match;
      });
  }

  ngOnInit() {
    this.getMatches();
  }

  editMatch(match: Match): void {
    match.editing = true;
  }

  stopEditMatch(match: Match): void {
    match.editing = false;
  }

  sendEditMatch( id, new_match_home_team_id, new_match_away_team_id, new_match_datetime, new_match_competition_id, new_match_result): void{
    this.matchService
      .update(
        id,
        new_match_home_team_id.nativeElement.value,
        new_match_away_team_id.nativeElement.value,
        new_match_datetime.nativeElement.value,
        new_match_competition_id.nativeElement.value,
        new_match_result.nativeElement.value
      )
      .then( (res) => {  console.log(res); this.getMatches(); });
  }

  addNewMatch( event, new_match_home_team_id, new_match_away_team_id, new_match_datetime, new_match_competition_id, new_match_result){
    event.preventDefault();
    this.matchService
      .create(new_match_home_team_id, new_match_away_team_id, new_match_datetime, new_match_competition_id, new_match_result)
      .then( (res) => {  console.log(res); this.getMatches(); });
  }

  deleteMatch(match: Match): void {
    match.deleting = true;
    this.match_deleting = match;
    this.getOdds(match.id);
  }

  getOdds(match_id){
    this.oddService
    .getOdds(match_id)
    .then(odds => {
      if(odds.length>0){
        this.odds_deleting = odds;
      }else{
        this.odds_deleting = undefined;
        this.receptor_match = undefined;
      }
    });
  }

  sendDeleteMatch(): void{
    this.matchService
      .delete(this.match_deleting.id)
      .then(e => {
        this.getMatches();
      });
    if(this.odds_deleting){
      this.odds_deleting.forEach(odd => {
        this.oddService
          .delete(odd.id);
      })
    }
    this.stopDeleteMatch();
  }

  stopDeleteMatch(){
    this.match_deleting = undefined;
    this.odds_deleting = undefined;
    this.receptor_match = undefined;
  }

  updateOdds(match_id){
    if(this.receptor_match
       && this.match_deleting.home_team_id == this.receptor_match.home_team_id
       && this.match_deleting.away_team_id == this.receptor_match.away_team_id ){
      this.odds_deleting.forEach(odd => {
      this.oddService
      .update(
        odd.id,
        match_id,
        odd.bet_type_id,
        odd.value,
        odd.datetime,
        odd.datetime_last,
        odd.sportsbook_id,
        odd.result
      )
      .then( (res) => { this.getOdds(this.match_deleting.id); });
      })
    }
  }

  showOdds(match: Match) {
    console.log(match.id);
  }
}
