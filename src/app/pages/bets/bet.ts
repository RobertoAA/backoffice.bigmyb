export class Bet {
    id: number; // INT 11 not null AUTO_INCREMENT - PRIMARY KEY
    odd_id: number;
    datetime: string; // VARCHAR 45 null (sin valor predeterminado)
    option_id: string;
    import: number;
    result: number;
    account_id: number;
    editing: boolean;
}
