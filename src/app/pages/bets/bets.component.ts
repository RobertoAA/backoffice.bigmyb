import { Component, OnInit, ViewChild } from '@angular/core';

import { Bet } from './bet';
import { BetService } from './bet.service';

@Component({
  selector: 'app-bets',
  templateUrl: './bets.component.html',
  styleUrls: ['./bets.component.css']
})
export class BetsComponent implements OnInit {
  @ViewChild('edit_bet_odd_id') edit_bet_odd_id;
  @ViewChild('edit_bet_datetime') edit_bet_datetime;
  @ViewChild('edit_bet_option_id') edit_bet_option_id;
  @ViewChild('edit_bet_import') edit_bet_import;
  @ViewChild('edit_bet_result') edit_bet_result;
  @ViewChild('edit_bet_account_id') edit_bet_account_id;
  
  bets: Bet[];

  constructor(
    private betService: BetService
  ) { }

  getBets(): void {
    this.betService
      .getBets()
      .then(bets => {
        this.bets = bets; 
        this.bets.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getBets();
  }
  
  editBet(bet: Bet){
    bet.editing = true;
  }
  
  stopEditBet(bet: Bet): void {
    bet.editing = false;
  }

  sendEditBet( id, new_bet_odd_id, new_bet_datetime, new_bet_option_id, new_bet_import, new_bet_result, new_bet_account_id): void{
    this.betService
      .update( 
        id,
        new_bet_odd_id.nativeElement.value, 
        new_bet_datetime.nativeElement.value, 
        new_bet_option_id.nativeElement.value, 
        new_bet_import.nativeElement.value,
        new_bet_result.nativeElement.value,
        new_bet_account_id.nativeElement.value
      )
      .then( (res)=>{  console.log(res); this.getBets(); });
  }

  addNewBet( event, new_bet_odd_id, new_bet_datetime, new_bet_option_id, new_bet_import, new_bet_result, new_bet_account_id): void{
    event.preventDefault();
    this.betService
      .create(new_bet_odd_id, new_bet_datetime, new_bet_option_id, new_bet_import, new_bet_result, new_bet_account_id)
      .then( (res)=>{ console.log(res); this.getBets(); });

  }
  
}
