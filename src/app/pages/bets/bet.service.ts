import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Bet } from './bet';

@Injectable()
export class BetService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost/api.bigmyb/public/bets';  // URL to web api
  private url_create = 'http://localhost/api.bigmyb/public/bet';  // URL to web api

  constructor(private http: Http) { }

  getBets(): Promise<Bet[]> {
    return this.http.get(this.url)
               .toPromise()
               // .then(response => console.log(response.json()))
               .then(response => response.json() as Bet[])
               .catch(this.handleError);
  }


  getBet(id: number): Promise<Bet> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Bet)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(odd_id: number, datetime: string, option_id: string, importe: number, result: number, account_id: number): Promise<Bet> {
    return this.http
      .post(this.url_create, JSON.stringify({odd_id: odd_id, datetime: datetime, option_id: option_id, import: importe, result: result, account_id: account_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Bet)
      .catch(this.handleError);
  }

  update(id: number, odd_id: number, datetime: string, option_id: string, importe: number, result: number, account_id: number): Promise<Bet> {
    const url = `${this.url_create}/${id}`;
    return this.http
      .put(url, JSON.stringify({id:id, odd_id: odd_id, datetime: datetime, option_id: option_id, import: importe, result: result, account_id: account_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Bet)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

