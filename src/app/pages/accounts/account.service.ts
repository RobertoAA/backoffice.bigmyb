import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Account } from './account';

@Injectable()
export class AccountService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost/api.bigmyb/public/accounts';  // URL to web api
  private url_create = 'http://localhost/api.bigmyb/public/account';  // URL to web api

  constructor(private http: Http) { }

  getAccounts(): Promise<Account[]> {
    return this.http.get(this.url)
               .toPromise()
               // .then(response => console.log(response.json()))
               .then(response => response.json() as Account[])
               .catch(this.handleError);
  }


  getAccount(id: number): Promise<Account> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Account)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(user_id: number, sportsbook_id: number): Promise<Account> {
    return this.http
      .post(this.url_create, JSON.stringify({user_id: user_id, sportsbook_id: sportsbook_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Account)
      .catch(this.handleError);
  }

  update(id: number, user_id: number, sportsbook_id: number): Promise<Account> {
    const url = `${this.url_create}/${id}`;
    return this.http
      .put(url, JSON.stringify({id: id, user_id: user_id, sportsbook_id: sportsbook_id}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Account)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

