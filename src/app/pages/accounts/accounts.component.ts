import { Component, OnInit, ViewChild } from '@angular/core';

import { Account } from './account';
import { AccountService } from './account.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  @ViewChild('edit_user_id') edit_user_id;
  @ViewChild('edit_sportsbook_id') edit_sportsbook_id;
  accounts: Account[];

  constructor(
    private accountService: AccountService
  ) { }

  getAccounts(): void {
    this.accountService
      .getAccounts()
      // .then( function (sportsbooks) { this.sportsbooks = sportsbooks; console.log(sportsbooks); });
      .then(accounts => {
        this.accounts = accounts; 
        this.accounts.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getAccounts();
  }

  editAccount(account: Account){
    account.editing = true;
  }
  
  stopEditAccount(account: Account): void {
    account.editing = false;
  }

  sendEditAccount( id, new_user_id, new_sportsbook_id): void{
    this.accountService
      .update( 
        id,
        new_user_id.nativeElement.value, 
        new_sportsbook_id.nativeElement.value
      )
      .then( (res)=>{  console.log(res); this.getAccounts(); });
  }

  addAccount( event, new_user_id, new_sportsbook_id): void{
    event.preventDefault();
    this.accountService
      .create(new_user_id, new_sportsbook_id)
      .then( (res)=>{ console.log(res); this.getAccounts(); });

  }

}
