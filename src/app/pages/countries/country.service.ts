import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Country } from './country';

@Injectable()
export class CountryService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost/api.bigmyb/public/countries';  // URL to web api
  private url_create = 'http://localhost/api.bigmyb/public/country';  // URL to web api

  constructor(private http: Http) { }

  getCountries(): Promise<Country[]> {
    return this.http.get(this.url)
               .toPromise()
               // .then(response => console.log(response.json()))
               .then(response => response.json() as Country[])
               .catch(this.handleError);
  }


  getCountry(id: number): Promise<Country> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Country)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string): Promise<Country> {
    return this.http
      .post(this.url_create, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Country)
      .catch(this.handleError);
  }

  update(id: number, name: string): Promise<Country> {
    const url = `${this.url_create}/${id}`;
    return this.http
      .put(url, JSON.stringify({id: id, name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Country)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

