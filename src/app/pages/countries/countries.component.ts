import { Component, OnInit, ViewChild } from '@angular/core';

import { Country } from './country';
import { CountryService } from './country.service';

@Component({
  selector: 'app-country',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {
  @ViewChild('edit_name') edit_name;
  countries: Country[];

  constructor(
    private CountryService: CountryService
  ) { }

  getCountries(): void {
    this.CountryService
      .getCountries()
      .then(countries => {
        this.countries = countries; 
        this.countries.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getCountries();
  }

  editCountry(country: Country){
    country.editing = true;
  }
  
  stopEditCountry(country: Country): void {
    country.editing = false;
  }

  sendEditCountry( id, new_name): void{
    this.CountryService
      .update( 
        id,
        new_name.nativeElement.value, 
      )
      .then( (res)=>{  console.log(res); this.getCountries(); });
  }

  addNewCountry( event, new_name): void{
    event.preventDefault();
    this.CountryService
      .create(new_name)
      .then( (res)=>{ console.log(res); this.getCountries(); });

  }

}
