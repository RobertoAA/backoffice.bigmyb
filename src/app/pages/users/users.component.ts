import { Component, OnInit, ViewChild } from '@angular/core';

import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @ViewChild('edit_name') edit_name;
  users: User[];

  constructor(
    private userService: UserService
  ) { }

  getUsers(): void {
    this.userService
      .getUsers()
      .then(users => {
        this.users = users; 
        this.users.forEach( function(obj){ obj.editing = false; } )
      });
  }

  ngOnInit() {
    this.getUsers();
  }

  editUser(user: User){
    user.editing = true;
  }
  
  stopEditUser(user: User): void {
    user.editing = false;
  }

  sendEditUser( id, new_name): void{
    this.userService
      .update( 
        id,
        new_name.nativeElement.value, 
      )
      .then( (res)=>{  console.log(res); this.getUsers(); });
  }

  addNewUser( event, new_name): void{
    event.preventDefault();
    this.userService
      .create(new_name)
      .then( (res)=>{ console.log(res); this.getUsers(); });

  }

}
