export class User {
    id: number;
    name: string;
    editing: boolean;
}
