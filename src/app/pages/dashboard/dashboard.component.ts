import { Component, OnInit } from '@angular/core';

import { Match } from './../matches/match';
import { MatchService } from './../matches/match.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  matches: Match[];

  constructor(
    private matchService: MatchService
  ) { }

  ngOnInit() {
    this.getMatches();
  }

  getMatches(): void {
    this.matchService
      .getMatches()
      // .then( function (sportsbooks) { this.sportsbooks = sportsbooks; console.log(sportsbooks); });
      .then(matches => {
        this.matches = matches;
        this.matches.forEach( function(obj){ obj.editing = false; } );
      });
  }

}
