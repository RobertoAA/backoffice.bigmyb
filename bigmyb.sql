-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bigmyb
CREATE DATABASE IF NOT EXISTS `bigmyb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bigmyb`;

-- Volcando estructura para tabla bigmyb.accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `sportsbook_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.accounts: 6 rows
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT IGNORE INTO `accounts` (`id`, `user_id`, `sportsbook_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 7),
	(4, 1, 10),
	(5, 1, 13),
	(6, 1, 15);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.alias_teams
CREATE TABLE IF NOT EXISTS `alias_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.alias_teams: 2 rows
/*!40000 ALTER TABLE `alias_teams` DISABLE KEYS */;
INSERT IGNORE INTO `alias_teams` (`id`, `team_id`, `name`, `web`) VALUES
	(1, 1, 'rm', 'betfair'),
	(2, 2, 'Barcelona', 'betfair');
/*!40000 ALTER TABLE `alias_teams` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.bets
CREATE TABLE IF NOT EXISTS `bets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `odd_id` int(11) NOT NULL DEFAULT '0',
  `datetime` varchar(50) DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  `import` varchar(50) NOT NULL DEFAULT '0',
  `result` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.bets: 0 rows
/*!40000 ALTER TABLE `bets` DISABLE KEYS */;
/*!40000 ALTER TABLE `bets` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.bet_types
CREATE TABLE IF NOT EXISTS `bet_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `options` varchar(200) NOT NULL DEFAULT '0',
  `sport_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.bet_types: 0 rows
/*!40000 ALTER TABLE `bet_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `bet_types` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.competitions
CREATE TABLE IF NOT EXISTS `competitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.competitions: 1 rows
/*!40000 ALTER TABLE `competitions` DISABLE KEYS */;
INSERT IGNORE INTO `competitions` (`id`, `name`) VALUES
	(1, 'Liga');
/*!40000 ALTER TABLE `competitions` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.countries: 1 rows
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT IGNORE INTO `countries` (`id`, `name`) VALUES
	(1, 'spain');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.matches
CREATE TABLE IF NOT EXISTS `matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `home_team_id` int(11) NOT NULL,
  `away_team_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `competition_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.matches: 2 rows
/*!40000 ALTER TABLE `matches` DISABLE KEYS */;
INSERT IGNORE INTO `matches` (`id`, `home_team_id`, `away_team_id`, `datetime`, `competition_id`) VALUES
	(1, 1, 2, '2017-11-10 22:00:00', 1),
	(3, 1, 2, '2017-11-12 22:00:00', 1);
/*!40000 ALTER TABLE `matches` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.odds
CREATE TABLE IF NOT EXISTS `odds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_id` int(11) NOT NULL DEFAULT '0',
  `bet_type_id` int(11) NOT NULL DEFAULT '0',
  `value` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sportsbook_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.odds: 0 rows
/*!40000 ALTER TABLE `odds` DISABLE KEYS */;
INSERT IGNORE INTO `odds` (`id`, `match_id`, `bet_type_id`, `value`, `datetime`, `sportsbook_id`) VALUES
	(1, 3, 1, '[2.3, 3.2, 3.8]', '2017-11-12 17:58:23', 1),
	(2, 3, 1, '[2.3, 3.2, 3.8]', '2017-11-12 17:58:23', 1);
/*!40000 ALTER TABLE `odds` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.sports
CREATE TABLE IF NOT EXISTS `sports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.sports: 3 rows
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
INSERT IGNORE INTO `sports` (`id`, `name`) VALUES
	(1, 'soccer'),
	(2, 'basketball'),
	(3, 'tennis');
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.sportsbooks
CREATE TABLE IF NOT EXISTS `sportsbooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.sportsbooks: 20 rows
/*!40000 ALTER TABLE `sportsbooks` DISABLE KEYS */;
INSERT IGNORE INTO `sportsbooks` (`id`, `name`) VALUES
	(1, 'marathon'),
	(2, 'betfair'),
	(3, '888sports'),
	(4, 'williamHill'),
	(5, 'interwetten'),
	(6, 'paston'),
	(7, 'marcaApuestas'),
	(8, 'wannabet'),
	(9, 'paf'),
	(10, 'sportium'),
	(11, 'bwin'),
	(12, 'suertia'),
	(13, 'bet365'),
	(14, 'luckia'),
	(15, 'titanbet'),
	(16, 'betStars'),
	(17, 'goldenPark'),
	(18, 'circus'),
	(19, 'codere'),
	(20, 'kirolbet');
/*!40000 ALTER TABLE `sportsbooks` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.teams
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `sport_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.teams: 2 rows
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT IGNORE INTO `teams` (`id`, `name`, `sport_id`, `country_id`) VALUES
	(1, 'Real Madrid', 1, 1),
	(2, 'FC Barcelona', 1, 1);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

-- Volcando estructura para tabla bigmyb.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bigmyb.users: 1 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `name`) VALUES
	(1, 'Roberto');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
